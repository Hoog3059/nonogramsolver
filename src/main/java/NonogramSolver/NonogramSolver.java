package NonogramSolver;

import java.util.ArrayList;

public class NonogramSolver {
    private Nonogram nonogram;
    private boolean finished = false;

    NonogramSolver(Nonogram nonogram){
        this.nonogram = nonogram;
    }

    public void solve(){
        String previousNonogramCells = "";
        while(!previousNonogramCells.equals(this.nonogram.toString())){
            previousNonogramCells = this.nonogram.toString();
            // Loop over rows.
            for (NonogramRowColumn row : this.nonogram.getRows()){
            	if(!row.isCompleted()){
            		checkAllCellsIfCompleted(row);
                    fillByCompareOverlap(row);
                    crossSpacesTooSmall(row);
                    if(!previousNonogramCells.equals(this.nonogram.toString())){
                        System.out.println(nonogram.toString());
                    }
            	}                
            }
            
            previousNonogramCells = this.nonogram.toString();
            // Loop over columns.
            for (NonogramRowColumn column : this.nonogram.getColumns()) {
            	if(!column.isCompleted()){
            		checkAllCellsIfCompleted(column);
                    fillByCompareOverlap(column);
                    crossSpacesTooSmall(column);
                    if(!previousNonogramCells.equals(this.nonogram.toString())){
                        System.out.println(nonogram.toString());
                    }
            	}                
            }
        }
        System.out.println("No further changes were made. Algorithm is incomplete or nonogram is unsolvable.");
    }
    
    // Check if the row/column is completed based on if all fill numbers have been satisfied. If that is the case, mark the row/column as completed.
    private void checkAllCellsIfCompleted(NonogramRowColumn rc){
        ArrayList<Integer> fillNumbers = rc.getFillNumbers();
        ArrayList<Integer> alreadyCountedIndices = new ArrayList<Integer>();
        int amountOfCompletedFillNumbers = 0;

        for(int fillNumber : fillNumbers){
            int currentSequenceLength = 0;
            ArrayList<Integer> countedIndices = new ArrayList<Integer>();
            for (int i = 0; i < rc.getLength(); i++) {
                NonogramCell cell = rc.getCell(i);

                NonogramCell.FILLTYPE fillType = cell.getFillType();
                if(fillType.equals(NonogramCell.FILLTYPE.FILLED) && !alreadyCountedIndices.contains(i)){
                    countedIndices.add(i);
                    currentSequenceLength++;
                }

                if(fillType.equals(NonogramCell.FILLTYPE.CROSSED) || fillType.equals(NonogramCell.FILLTYPE.EMPTY) || (i + 1 == rc.getLength()) || alreadyCountedIndices.contains(i)){
                    if(currentSequenceLength == fillNumber){
                        alreadyCountedIndices.addAll(countedIndices);
                        amountOfCompletedFillNumbers++;
                        currentSequenceLength = 0;
                        break;
                    }else{
                        currentSequenceLength = 0;
                    }
                }
            }
        }

        if(amountOfCompletedFillNumbers == fillNumbers.size()){
            for(NonogramCell cell : rc.getCells()){
                if(cell.getFillType().equals(NonogramCell.FILLTYPE.EMPTY)){
                    cell.fill(NonogramCell.FILLTYPE.CROSSED, false);
                }
            }
            rc.setCompleted(true);
            System.out.println("Row/column " + rc.hashCode() + " has been marked as completed. All empty cells were crossed.");
        }
    }

    private void fillByCompareOverlap(NonogramRowColumn rc){
        for (int i = 0; i < rc.getFillNumbers().size(); i++) {
        	int unavailableRangeFromShortSide = 0;
        	int unavailableRangeFromFarSide = 0;        	
        	
        	for (int cellIndex = rc.getLength() - 1; cellIndex > -1; cellIndex--) {
				if(rc.getCell(cellIndex).getFillType().equals(NonogramCell.FILLTYPE.EMPTY)){
					break;
				}else if(rc.getCell(cellIndex).getFillType().equals(NonogramCell.FILLTYPE.CROSSED)){
					unavailableRangeFromFarSide++;
				}else if(rc.getCell(cellIndex).getFillType().equals(NonogramCell.FILLTYPE.FILLED)){
					break;
				}
			}
        	
        	for (int cellIndex = 0; cellIndex < rc.getLength(); cellIndex++) {
				if(rc.getCell(cellIndex).getFillType().equals(NonogramCell.FILLTYPE.EMPTY)){
					break;
				}else if(rc.getCell(cellIndex).getFillType().equals(NonogramCell.FILLTYPE.CROSSED)){
					unavailableRangeFromShortSide++;
				}else if(rc.getCell(cellIndex).getFillType().equals(NonogramCell.FILLTYPE.FILLED)){
					break;
				}
			}
        	
        	
            if((rc.getFillNumbers().get(i) - (rc.getLength() - unavailableRangeFromFarSide - unavailableRangeFromShortSide - rc.sumOfFillNumbers() - rc.getFillNumbers().size() + 1)) > 0){
                int minRangeBegin = 0;
                for (int j = 0; j < i; j++) {
                    minRangeBegin += rc.getFillNumbers().get(j);
                    minRangeBegin += 1;
                }
                minRangeBegin += unavailableRangeFromShortSide;

                int minRangeEnd = minRangeBegin + rc.getFillNumbers().get(i);

                int maxRangeBegin = minRangeBegin + (rc.getLength() - unavailableRangeFromFarSide - unavailableRangeFromShortSide - rc.sumOfFillNumbers() - rc.getFillNumbers().size() + 1);
                int maxRangeEnd = maxRangeBegin + rc.getFillNumbers().get(i);

                int sureRangeBegin = maxRangeBegin;
                int sureRangeEnd = minRangeEnd;

                for (int j = sureRangeBegin; j < sureRangeEnd; j++) {
                    rc.getCell(j).fill(NonogramCell.FILLTYPE.FILLED, false);
                    System.out.println("");
                }
            }
        }
    }
    
    private void crossSpacesTooSmall(NonogramRowColumn rc){
    	
    }
}
