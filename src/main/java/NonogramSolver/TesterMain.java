package NonogramSolver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class TesterMain {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);

        System.out.println("X:");
        int sizeX = sc.nextInt();

        System.out.println("Y:");
        int sizeY = sc.nextInt();

        System.out.println("FN:");
        int fillNumberAmount = sc.nextInt();

        // String json = new String(Files.readAllBytes(Paths.get("C:\\Users\\timo\\Documents\\Projecten\\Java\\Java\\NonogramSolver\\15x15_test.json")));
        String json = new String(Files.readAllBytes(Paths.get("C:\\Users\\pc20\\Documents\\Hoogenbosch\\nonogramsolver\\10x10_test.json")));
    
        Nonogram nonogram = Nonogram.fromJSON(json);
        // Nonogram nonogram = new Nonogram(sizeX, sizeY, fillNumberAmount);
        // System.out.println(nonogram.toJSON());
        NonogramSolver nonogramSolver = new NonogramSolver(nonogram);
        nonogramSolver.solve();
        /*while(true){
            System.out.println(nonogram.toString());
        }*/
    }
}
