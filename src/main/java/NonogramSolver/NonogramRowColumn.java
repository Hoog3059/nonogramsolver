package NonogramSolver;

import java.util.ArrayList;

public class NonogramRowColumn {
    private transient boolean completed = false;
    private ArrayList<Integer> fillNumbers = new ArrayList<Integer>();
    private transient ArrayList<NonogramCell> cells = new ArrayList<NonogramCell>();

    public void addFillNumber(int nr){
        this.fillNumbers.add(nr);
    }

    public ArrayList<Integer> getFillNumbers(){
        return this.fillNumbers;
    }

    public void addCell(NonogramCell cell){
        this.cells.add(cell);
    }

    public ArrayList<NonogramCell> getCells(){
        return this.cells;
    }

    public NonogramCell getCell(int index){
        return this.cells.get(index);
    }

    public int getLength(){
        return this.cells.size();
    }

    public int sumOfFillNumbers(){
        int sum = 0;
        for (int fillNumber : this.fillNumbers){
            sum += fillNumber;
        }
        return sum;
    }
    
    public boolean isCompleted(){
    	return this.completed;
    }

    public void setCompleted(boolean completed){
        this.completed = completed;
    }
}
