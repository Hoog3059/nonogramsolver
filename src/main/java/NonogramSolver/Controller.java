package NonogramSolver;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;

public class Controller {
    //@FXML
    //public Canvas canvas;

    @FXML
    public GridPane nonogram;

    @FXML
    public TextField txtSizeX;

    @FXML
    public TextField txtSizeY;

    @FXML
    public TextField txtFillNumber;

    @FXML
    public Button btnApply;

    @FXML
    public Button btnStart;

    @FXML
    public Button btnStop;

    @FXML
    public void btnApplyOnMouseClicked(Event e){
        int sizeX = Integer.parseInt(txtSizeX.getText());
        int sizeY = Integer.parseInt(txtSizeY.getText());
        int fillNumberAmount = Integer.parseInt(txtFillNumber.getText());

        for(ColumnConstraints colConst : nonogram.getColumnConstraints()){
            nonogram.getColumnConstraints().remove(colConst);
        }
        for(RowConstraints rowConst : nonogram.getRowConstraints()){
            nonogram.getRowConstraints().remove(rowConst);
        }

        for (int i = 0; i < sizeX; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100.0 / sizeX);
            nonogram.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < sizeY; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100.0 / sizeY);
            nonogram.getRowConstraints().add(rowConst);
        }

        /*
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.setFill(Color.BLACK);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(1);

        // Upper line
        gc.strokeLine(10, 10, 10 + (sizeX*101) + 1, 10);

        // Bottom line
        gc.strokeLine(10, 10 + (sizeY*101), 10 + (sizeX*101) + 1, 10 + (sizeY*101));

        // Left line
        gc.strokeLine(10, 10, 10, 10 + (sizeY*101));

        // Right line
        gc.strokeLine(10 + (sizeX*101) + 1, 10, 10 + (sizeX*101) + 1, 10 + (sizeY*101));

        // Lines in between
        for (int i = 1; i < sizeX; i++) {
            gc.strokeLine(10 + (i*101), 10, 10 + (i*101), 10 + (sizeY*101));
        }
        for (int i = 1; i < sizeY; i++) {
            gc.strokeLine(10, 10 + (i*101), 11 + (sizeX*101), 10 + (i*101));
        }*/
    }

    @FXML
    public void btnStartOnMouseClicked(Event e){
        txtSizeX.setDisable(true);
        txtSizeY.setDisable(true);
        txtFillNumber.setDisable(true);
        btnApply.setDisable(true);
        btnStart.setDisable(true);
        btnStop.setDisable(false);

        int sizeX = Integer.parseInt(txtSizeX.getText());
        int sizeY = Integer.parseInt(txtSizeY.getText());
        int fillNumberAmount = Integer.parseInt(txtFillNumber.getText());

        Nonogram nonogram = new Nonogram(sizeX, sizeY, fillNumberAmount);
        NonogramSolver nonogramSolver = new NonogramSolver(nonogram);
        nonogramSolver.solve();

        /*GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.GREEN);
        gc.setStroke(Color.GREEN);
        gc.fillRect(10, 10, 20, 20);*/
    }

    @FXML
    public void btnStopOnMouseClicked(Event e){
        txtSizeX.setDisable(false);
        txtSizeY.setDisable(false);
        txtFillNumber.setDisable(false);
        btnApply.setDisable(false);
        btnStart.setDisable(false);
        btnStop.setDisable(true);
    }
}
