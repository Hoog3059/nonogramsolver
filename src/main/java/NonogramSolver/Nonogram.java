package NonogramSolver;

// import javafx.scene.canvas.Canvas;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Nonogram {
    private int sizeX;
    private int sizeY;
    private int fillNumberAmount;
    private ArrayList<NonogramRowColumn> rows = new ArrayList<NonogramRowColumn>();
    private ArrayList<NonogramRowColumn> columns = new ArrayList<NonogramRowColumn>();
    private transient NonogramCell[][] nonogramCells;

    Nonogram(int sizeX, int sizeY, int fillNumberAmount){
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.fillNumberAmount = fillNumberAmount;        

        // Create all cells
        this.createAllCells(sizeX, sizeY);

        loadNonogramCellsIntoRowsAndColumns(this.nonogramCells);

        // GC
        // nonogramCells = null;
    }

    @Deprecated
    public void draw(){

    }

    public NonogramCell[][] getCells(){
        return this.nonogramCells;
    }

    
    private void createAllCells(int sizeX, int sizeY){
    	this.nonogramCells = new NonogramCell[sizeX][sizeY];
    	
    	for(NonogramCell[] row : this.nonogramCells){
            for (int i = 0; i < this.sizeY; i++) {
                row[i] = new NonogramCell();
            }
        }
    }
    
    public ArrayList<NonogramRowColumn> getRows(){
        return this.rows;
    }

    public ArrayList<NonogramRowColumn> getColumns(){
        return this.columns;
    }

    public NonogramRowColumn getRow(int index){
        return this.rows.get(index);
    }

    public NonogramRowColumn getColumn(int index){
        return this.columns.get(index);
    }

    public int getSizeX(){
        return this.sizeX;
    }

    public int getSizeY(){
        return this.sizeY;
    }

    public String toString(){
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < this.fillNumberAmount; i++) {
            // Java 11
        	// output.append("  ".repeat(fillNumberAmount));
        	
        	// Java 8
            output.append(new String(new char[this.fillNumberAmount]).replace("\0", "  "));
            for(NonogramRowColumn column : this.columns){
                if(i < column.getFillNumbers().size()){
                    output.append(column.getFillNumbers().get(i)).append(" ");
                }else{
                    output.append("  ");
                }
            }
            output.append("\n");
        }
        for(NonogramRowColumn row : this.rows){
            for(int fillNumber : row.getFillNumbers()){
                output.append(fillNumber).append(" ");
            }
            // Java 11
            // output.append("  ".repeat(this.fillNumberAmount - row.getFillNumbers().size()));
            
            // Java 8
            output.append(new String(new char[this.fillNumberAmount - row.getFillNumbers().size()]).replaceAll("\0", "  "));
            for(NonogramCell cell : row.getCells()){
                if(cell.getFillType().equals(NonogramCell.FILLTYPE.EMPTY)){
                    output.append("□ ");
                }else if(cell.getFillType().equals(NonogramCell.FILLTYPE.CROSSED)){
                    output.append("x ");
                }else if(cell.getFillType().equals(NonogramCell.FILLTYPE.FILLED)){
                    output.append("■ ");
                }
            }
            output.append("\n");
        }
        return output.toString();
    }

    public String toJSON(){
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();

        Gson gson = builder.create();
        return gson.toJson(this);
    }

    private void loadNonogramCellsIntoRowsAndColumns(NonogramCell[][] cells){
        // Fill rows with cells.
        for (int Irow = 0; Irow < cells.length; Irow++) {
            NonogramRowColumn row;
            if(this.rows.size() > Irow){
                row = this.rows.get(Irow);
                row.getCells().removeAll(row.getCells());
            }else{
                row = new NonogramRowColumn();
            }

            for(NonogramCell cell : cells[Irow]){
                row.addCell(cell);
            }
            if(this.rows.size() > Irow){
                this.rows.set(Irow, row);
            }else{
                this.rows.add(row);
            }
        }

        /*for(NonogramCell[] row : cells){
            NonogramRowColumn new_row = new NonogramRowColumn();
            for(NonogramCell cell : row){
                new_row.addCell(cell);
            }
            this.rows.add(new_row);
        }*/

        // Fill columns variable with cells.
        for (int Icolumn = 0; Icolumn < cells[0].length; Icolumn++) {
            NonogramRowColumn column;
            if(this.columns.size() > Icolumn){
                column = this.columns.get(Icolumn);
                column.getCells().removeAll(column.getCells());
            }else{
                column = new NonogramRowColumn();
            }

            for (int Irow = 0; Irow < this.sizeY; Irow++) {
                column.addCell(cells[Irow][Icolumn]);
            }

            if(this.columns.size() > Icolumn){
                this.columns.set(Icolumn, column);
            }else{
                this.columns.add(column);
            }
        }

        /*for (int Icolumn = 0; Icolumn < this.sizeY; Icolumn++) {
            NonogramRowColumn new_column = new NonogramRowColumn();
            for (int Irow = 0; Irow < this.sizeX; Irow++) {
                new_column.addCell(cells[Irow][Icolumn]);
            }
            this.columns.add(new_column);
        }*/
    }

    public static Nonogram fromJSON(String json){
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();

        Gson gson = builder.create();
        Nonogram nonogram = gson.fromJson(json, Nonogram.class);
        nonogram.createAllCells(nonogram.getSizeX(), nonogram.getSizeY());
        nonogram.loadNonogramCellsIntoRowsAndColumns(nonogram.nonogramCells);

        return nonogram;
    }
}
