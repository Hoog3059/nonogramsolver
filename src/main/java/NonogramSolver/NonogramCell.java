package NonogramSolver;

public class NonogramCell {
    public enum FILLTYPE{
        EMPTY,
        FILLED,
        CROSSED
    }

    private FILLTYPE currentFill = FILLTYPE.EMPTY;

    public FILLTYPE getFillType(){
        return this.currentFill;
    }

    public void fill(FILLTYPE fill, boolean ignoreChanging) {
        if(this.currentFill.equals(FILLTYPE.EMPTY)){
            this.currentFill = fill;
        }else{
            if(((this.currentFill.equals(FILLTYPE.CROSSED) && fill.equals(FILLTYPE.FILLED)) || (this.currentFill.equals(FILLTYPE.FILLED) && fill.equals(FILLTYPE.CROSSED))) && !(ignoreChanging)){
                System.out.println("Tried to change unchangable filltype " + currentFill + " to " + fill);
            }else{
                this.currentFill = fill;
            }
        }
    }
}
